# README #

This is a submission for the fetch rewards programming task located here:
	http://fetch-hiring.s3.amazonaws.com/points.pdf
	

#### This project uses
*  Python 3.9.7: https://www.python.org/downloads/
*  Flask: https://flask.palletsprojects.com/en/2.0.x/
	
## How do I get set up?
### Package installation
#### Install the following python packages:
  * pip install virtualenv
  * pip install flask
  * pip install requests
	
I've installed this on Windows, this was all the setup that was required.
	
### How to run tests
	
Run fetch_server.py (the main submission) concurrently with any test script (located in test_cases directory).
	
### Design ###
Transaction data is stored in a list of transaction_info objects.

The list is kept sorted according to timestamp.

The list is iterated through and items are modified or removed when spending is requested.

### Usage ###
The server accepts POST requests at URLs specified in the testfuncs.py file:

  * http://localhost:5000/transaction/<uuid\> for transactions (POST)
  * http://localhost:5000/spendPoints/<uuid\> for spending (POST)
  * http://localhost:5000/getAllBalances for getting payer balances (GET)
  * http://localhost:5000/clearAll for clearing balances (POST)
    * This was primarily implemented to ease testing.
