from testfuncs import *

if __name__ == '__main__':
    res = requests.post(clear_url)
    
    #add a few, spend, add some more, spend, etc
    transaction = { "payer": "ABC", "points": 100, "timestamp": "2020-10-01T11:00:00Z" },
    sendTransactions(transaction);
    transaction = { "payer": "DEF", "points": 2000, "timestamp": "2020-10-02T11:00:00Z" },
    sendTransactions(transaction);
    transaction = { "payer": "GHI", "points": 333, "timestamp": "2020-10-03T11:00:00Z" },
    sendTransactions(transaction);
    transaction = { "payer": "JKL", "points": 4, "timestamp": "2020-10-04T11:00:00Z" },
    sendTransactions(transaction);
    
    expected_spendresult = [
        { "payer": "ABC", "points": -100 }, 
        { "payer": "DEF", "points": -2000 }, 
        { "payer": "GHI", "points": -200 }
    ]
    sendAndVerifySpend({ "points": 2300 }, expected_spendresult);
    
    expected_balanceresult = { 
        "ABC": 0,
        "DEF": 0,
        "GHI": 133,
        "JKL": 4
    }
    getAndVerifyBalance(expected_balanceresult)
    
    # now add back the old values, but make sure we spend the new ones first
    transaction = { "payer": "ABC", "points": 100, "timestamp": "2020-10-05T11:00:00Z" },
    sendTransactions(transaction);
    transaction = { "payer": "DEF", "points": 2000, "timestamp": "2020-10-06T11:00:00Z" },
    sendTransactions(transaction);
    expected_spendresult = [
        { "payer": "GHI", "points": -133 }, 
        { "payer": "JKL", "points": -4 }, 
        { "payer": "ABC", "points": -50 }
    ]
    sendAndVerifySpend({ "points": 187 }, expected_spendresult);
    
    expected_balanceresult = { 
        "ABC": 50,
        "DEF": 2000,
        "GHI": 0,
        "JKL": 0
    }
    getAndVerifyBalance(expected_balanceresult)
    
    #add transaction in the past, make sure that gets spent first, then ABC, then DEF
    transaction = { "payer": "GHI", "points": 333, "timestamp": "2020-09-05T11:00:00Z" },
    sendTransactions(transaction);
    expected_spendresult = [
        { "payer": "GHI", "points": -333 }, 
        { "payer": "ABC", "points": -50 }, 
        { "payer": "DEF", "points": -500 }
    ]
    sendAndVerifySpend({ "points": 883 }, expected_spendresult);
    
    expected_balanceresult = { 
        "ABC": 0,
        "DEF": 1500,
        "GHI": 0,
        "JKL": 0
    }
    getAndVerifyBalance(expected_balanceresult)