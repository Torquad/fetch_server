from testfuncs import *

def jsonMissingPayer():
    giveXYZ = [{ "points": 1234, "timestamp": "2020-11-02T14:00:00Z" }]
    sendTransactions(giveXYZ, True);
    
def jsonMissingPoints():
    giveXYZ = [{ "payer": "XYZ", "timestamp": "2020-11-02T14:00:00Z" }]
    sendTransactions(giveXYZ, True);
    
def jsonMissingTimestamp():
    giveXYZ = [{ "payer": "XYZ", "points": 1234}]
    sendTransactions(giveXYZ, True);

def spendJsonMissingTimestamp():
    giveXYZ = [{ "payer": "XYZ", "points": 1111, "timestamp": "2020-11-02T14:00:00Z" }]
    sendTransactions(giveXYZ);
    sendAndVerifySpend({ }, [], True);

if __name__ == '__main__':
    res = requests.post(clear_url)
    jsonMissingPayer()
    jsonMissingPoints()
    jsonMissingTimestamp()
    spendJsonMissingTimestamp()