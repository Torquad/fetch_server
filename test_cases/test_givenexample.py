import requests
import json

# keep uuid here?
transaction_url = 'http://localhost:5000/transaction/1234'
spend_url = 'http://localhost:5000/spendPoints/1234'
getbalance_url = 'http://localhost:5000/getAllBalances'
clear_url = 'http://localhost:5000/clearAll'

# first, try provided test cases
transactions = [
    { "payer": "DANNON",        "points": 1000,     "timestamp": "2020-11-02T14:00:00Z" },
    { "payer": "UNILEVER",      "points": 200,      "timestamp": "2020-10-31T11:00:00Z" },
    { "payer": "DANNON",        "points": -200,     "timestamp": "2020-10-31T15:00:00Z" },
    { "payer": "MILLER COORS",  "points": 10000,    "timestamp": "2020-11-01T14:00:00Z" },
    { "payer": "DANNON",        "points": 300,      "timestamp": "2020-10-31T10:00:00Z" }
]

expected_spendresult = [
    { "payer": "DANNON", "points": -100 },
    { "payer": "UNILEVER", "points": -200 },
    { "payer": "MILLER COORS", "points": -4700 }
]

expected_balanceresult = { 
    "DANNON": 1000, 
    "UNILEVER": 0, 
    "MILLER COORS": 5300 
}

spend_request = { "points": 5000 }



if __name__ == '__main__':
    res = requests.post(clear_url)
    
    for t in transactions:
        res = requests.post(transaction_url, json=t)
        # when posting a transaction, no response data required to check
        if res.ok:
            print("post transaction success")
        else:
            print("post transaction failed")
    
    # Now test spend
    res = requests.post(spend_url, json=spend_request)
    
    actual_spendresult = res.json()
    if(expected_spendresult == actual_spendresult):
        print("post spend success") 
    else:
        print("post spend failed")
    
    # Now test get balance
    res = requests.get(getbalance_url);
    actual_balanceresult = res.json()
    if(expected_balanceresult == actual_balanceresult):
        print("get balance success") 
    else:
        print("get balance failed")