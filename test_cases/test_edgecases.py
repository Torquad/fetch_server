from testfuncs import *

def edgeTest1():
    print("Test 1");
    #give XYZ some points
    giveXYZ = [{ "payer": "XYZ", "points": 1000, "timestamp": "2020-11-02T14:00:00Z" }]
    sendTransactions(giveXYZ);
    
    #get all(): XYZ with current points, no ABC
    giveXYZ_result = { 
        "XYZ": 1000,
    }
    getAndVerifyBalance(giveXYZ_result)
    
    #give ABC 0 pts
    giveABC = { "payer": "ABC", "points": 0, "timestamp": "2020-10-31T11:00:00Z" },
    sendTransactions(giveABC);
    #get all(): XYZ with current points, ABC with 0
    giveABC_result = { 
        "XYZ": 1000,
        "ABC": 0,
    }
    getAndVerifyBalance(giveABC_result)

def edgeTest2():
    print("Test 2");
    #give XYZ n points
    giveXYZ = [{ "payer": "XYZ", "points": 1234, "timestamp": "2020-11-02T14:00:00Z" }]
    sendTransactions(giveXYZ);
    #give ABC m points
    giveABC = [{ "payer": "ABC", "points": 2345, "timestamp": "2020-11-03T14:00:00Z" }]
    sendTransactions(giveXYZ);
    #spend >(n+m) points
    #leave the expected result empty, test should fail
    sendAndVerifySpend({ "points": 1234+2345+1000 }, [], True);
    #fail
def edgeTest3():
    print("Test 3");
    #spend negative points
    sendAndVerifySpend({ "points": -100 }, [], True);
    #fail

def edgeTest4():
    print("Test 4");
    #give multiple transactions all with same timestamp
    #give XYZ n points
    giveXYZ = [{ "payer": "XYZ", "points": 1234, "timestamp": "2020-11-02T14:00:00Z" }]
    sendTransactions(giveXYZ);
    #give XYZ n points
    giveXYZ = [{ "payer": "XYZ", "points": 1111, "timestamp": "2020-11-02T14:00:00Z" }]
    sendTransactions(giveXYZ);
    sendAndVerifySpend({ "points": 2345}, [{ "payer": "XYZ", "points": -2345 }]);

if __name__ == '__main__':
    res = requests.post(clear_url)
    edgeTest1()
    res = requests.post(clear_url)
    edgeTest2()
    res = requests.post(clear_url)
    edgeTest3()
    res = requests.post(clear_url)
    edgeTest4()
    res = requests.post(clear_url)