import requests
import json

transaction_url = 'http://localhost:5000/transaction/1234'
spend_url = 'http://localhost:5000/spendPoints/1234'
getbalance_url = 'http://localhost:5000/getAllBalances'
clear_url = 'http://localhost:5000/clearAll'

def sendTransactions(transactions, shouldFail = False):
    for t in transactions:
        res = requests.post(transaction_url, json=t)
        # when posting a transaction, no response data required to check
        if res.ok and not shouldFail:
            print("post transaction success")
        elif not res.ok and shouldFail:
            print("post transaction successfully failed")
        else:
            print("post transaction failed")

def sendAndVerifySpend(spend_request, expected_spendresult, shouldFail = False):
    # Now test spend
    res = requests.post(spend_url, json=spend_request)
    
    if not res.ok and shouldFail:
        print("post transaction successfully failed")
    elif not res.ok:
        print("post spend failed")
    else:
        actual_spendresult = res.json()
        if(expected_spendresult == actual_spendresult):
            print("post spend success") 
        else:
            print("post spend failed")

def getAndVerifyBalance(expected_balanceresult):
    # Now test get balance
    res = requests.get(getbalance_url);
    actual_balanceresult = res.json()
    if(expected_balanceresult == actual_balanceresult):
        print("get balance success") 
    else:
        print("get balance failed")
