from flask import Flask, redirect, url_for, request, Response, json, abort
import time

app = Flask(__name__)

'''
Assumptions:
    1. We only check for negative balances on spend operations. If we check on transactions, there's no way of knowing whether a transaction will be added later (with an earlier timestamp) that will resolve the "negative balance" issue.
    2. We can't spend negative points.
    3. No login or security analysis is in scope for this task.
'''

## Class/POD struct representing transaction data
class TransactionInfo:
    def __init__(self, timestamp, payer, points):
        self.m_timestamp = timestamp
        self.m_payer = payer
        self.m_points = points

## Class that represents a user & all its payers
class User:
    def __init__(self):
        self.m_transactionList = []
        self.m_totalPoints = 0
        self.m_payerList = []
    def clearAll(self):
        self.m_transactionList = []
        self.m_totalPoints = 0
        self.m_payerList = []
        return "success"
    #calculate the total amount of points for a specific payer.
    def calcPayerTotal(self, payer):
        total = 0
        for entry in self.m_transactionList:
            if(entry.m_payer == payer):
                total += entry.m_points
        return total
    #update all class members in accordance with new transaction information
    def addTransaction(self, content):
        #fail immediately if the proper data is not supplied
        if(not 'payer' in content or not 'points' in content or not 'timestamp' in content):
            abort(400, Response("Missing required json field"))
        
        #keep track of all payers
        if(content['payer'] not in self.m_payerList):
            self.m_payerList.append(content['payer'])
        
        #don't bother updating any structures (aside from the payer list) for a 0 point transaction
        if(content['points'] == 0):
            return "success"
        
        #add to the list
        self.m_transactionList.append(TransactionInfo(content['timestamp'], content['payer'], content['points']))
        
        #keep list sorted by timestamp
        self.m_transactionList.sort(key=lambda x:time.mktime(time.strptime(x.m_timestamp, '%Y-%m-%dT%H:%M:%SZ')))
        
        self.m_totalPoints += content['points']
        return "success"
        
    def spendPoints(self, points_to_deduct):
        #I'm assuming that this is not allowed, it's not a unrealistic assumption
        if(points_to_deduct < 0):
            abort(400, Response("Can't spend negative points"))
        
        # save time looping through data structure by saving total points and failing if we try to spend more.
        # we could also loop through the payer list and sum this up on the fly.
        if(points_to_deduct > self.m_totalPoints):
            abort(400, Response("Not enough points"))
        
        #keep track of all deductions so we can return this information at the end.
        payer_deductions = {}
        for entry in self.m_transactionList[:]:
            #if we're done deducting, exit the loop
            if(points_to_deduct == 0):
                break
            
            payerTotal = self.calcPayerTotal(entry.m_payer)
            #if this payer is out of points, skip this entry
            if(payerTotal == 0):
                continue
            
            #decide how many points we can deduct from this entry
            entry_deduction = min(points_to_deduct, entry.m_points, payerTotal)
            
            #either add a new element to the map or sum the deduction with the previous ones
            if(entry.m_payer not in payer_deductions):
                payer_deductions[entry.m_payer] = entry_deduction
            else:
                payer_deductions[entry.m_payer] += entry_deduction
            
            #update all members to reflect the deduction
            points_to_deduct -= entry_deduction
            entry.m_points -= entry_deduction
            self.m_totalPoints -= entry_deduction
            
            #remove entries with 0 points left
            if(entry.m_points == 0):
                self.m_transactionList.remove(entry)
            
        # success
        #build json from list
        if(points_to_deduct == 0):
            json_response = []
            for key in payer_deductions:
                json_entry = {}
                json_entry['payer'] = key
                json_entry['points'] = -payer_deductions[key]
                json_response.append(json_entry)
            return json.dumps(json_response)
        
        # failure
        else:
            abort(Response("Not enough points (this should have been caught earlier)"))
    
    # calculate and return point information on balances
    def getAllBalances(self):
        json_response = {}
        for payer in self.m_payerList:
            json_response[payer] = self.calcPayerTotal(payer)
        return json.dumps(json_response)

user = User()

## ROUTING
@app.route('/transaction/<uuid>', methods=['POST'])
def handleTransaction(uuid):
    content = request.json
    user.addTransaction(content)
    return getAllBalances()

@app.route('/spendPoints/<uuid>', methods=['POST'])
def spendPoints(uuid):
    content = request.json
    if(not 'points' in content):
            abort(400, Response("Missing required json field"))
    return user.spendPoints(content['points'])

@app.route('/getAllBalances', methods=['GET'])
def getAllBalances():
    return user.getAllBalances()
    
@app.route('/clearAll', methods=['POST'])
def clearAll():
    # clear all transaction data (useful for testing), should be protected IRL
    return user.clearAll()

## MAIN
if __name__ == '__main__':
    app.run()
    


